<?php

use Drupal\Core\Url;


/**
 * Implements hook_theme().
 */
function ws_promotion_cards_theme($existing, $type, $theme, $path) {
  return [
    'node__promo__lb_cards_standard' => [
      'base hook' => 'node',
      'template' => 'node--promo--lb-cards-standard',
    ],
    'node__promo__lb_cards_chevron' => [
      'base hook' => 'node',
      'template' => 'node--promo--lb-cards-chevron',
    ],
    'node__promo__lb_cards_color' => [
      'base hook' => 'node',
      'template' => 'node--promo--lb-cards-color',
    ],
    'node__promo__lb_cards_overlay' => [
      'base hook' => 'node',
      'template' => 'node--promo--lb-cards-overlay',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function ws_promotion_cards_preprocess_block__lb_cards(&$variables) {
  $configuration = $variables['elements']['#configuration'];
  if (empty($configuration['promotion_include'])) {
    return;
  }
  $block = $variables['block_content'];
  $variables['#attached']['library'][] = 'ws_promotion_cards/insert_promotion';
  $paramsRoute = ['widget' => $configuration['block_variation'] ?? $variables['derivative_plugin_id']];
  if (!empty($configuration['promotion_category'])) {
    $paramsRoute['category'] = $configuration['promotion_category'];
  }
  $variables['#attached']['drupalSettings']['ws_promotion_cards'][$block->getRevisionId()] = [
    'item_number' => 0,
    'url' => Url::fromRoute('ws_promotion.get_promo', $paramsRoute, ['absolute' => TRUE])->toString(),
  ];
}

/**
 * Implements hook_promotion_block_list_alter().
 * Add the name of the block to the list for the promotion alter form.
 */
function ws_promotion_cards_promotion_block_list_alter(array &$listOfBlocks) {
  $listOfBlocks[] = 'lb_cards';
}

/**
 * Implements hook_promotion_preview_list_alter().
 * Add the name of the preview template to the list for the promotion preview.
 */
function ws_promotion_cards_promotion_preview_list_alter(array &$listOfPreview) {
  $listOfPreview['lb_cards_standard'] = t('Cards (standard)');
  $listOfPreview['lb_cards_chevron'] = t('Cards (chevron)');
  $listOfPreview['lb_cards_overlay'] = t('Cards (overlay)');
  $listOfPreview['lb_cards_color'] = t('Cards (color)');
}
